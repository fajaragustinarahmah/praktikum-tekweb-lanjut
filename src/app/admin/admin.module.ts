import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Router, RouterModule, ROUTES, Routes } from '@angular/router';
import { MaterialDesign } from '../material/material';
import { ImagesComponent } from './images/images.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  {
    path:'dashboard',
    component:DashboardComponent
  },
  {
    path:'images',
    component:ImagesComponent
  },
  //pengaturan router untuk halaman product
  {
    path:'product',
    component:ProductComponent
  }, 
  {
    path:'',
    pathMatch:'full',
    redirectTo:'/admin/dashboard'
  }
]

@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    ImagesComponent,
    ProductComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialDesign
  ]
})
export class AdminModule { }
